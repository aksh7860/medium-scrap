# README #

Follow the Steps to Set Up Web Scrapping Tool.

### What is this repository for? ###

* This is small web scrapping tool for medium blog 

### How do I get set up? ###

* git clone https://aksh7860@bitbucket.org/aksh7860/medium-scrap.git
* cd medium-scrap
* npm install
* Install mysql and execute the scrapWebsite.sql
* Open Browser and access the url http://localhost:3000/api/scrapData


### Who do I talk to? ###

* Abhishek Kumar (abhi.kumar793@gmail.com)